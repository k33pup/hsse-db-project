-- Создание схемы базы данных блокчейна
CREATE SCHEMA blockchain_database;


-- Информация о монетах
CREATE TABLE blockchain_database.coin (
  coin_id SERIAL PRIMARY KEY,
  coin_name TEXT NOT NULL,
  coin_project_url TEXT NOT NULL
);

-- Информация об узлах
CREATE TABLE blockchain_database.node (
  node_pub_key varchar(64) PRIMARY KEY,
  node_private_key varchar(64) NOT NULL UNIQUE,
  join_date TIMESTAMP NOT NULL,
  operations_cnt INTEGER NOT NULL
);

-- Информация о майнерах
CREATE TABLE blockchain_database.miner (
  miner_pub_key varchar(64) PRIMARY KEY NOT NULL REFERENCES blockchain_database.node(node_pub_key),
  block_mined_cnt INTEGER NOT NULL,
  miner_power DOUBLE PRECISION NOT NULL
);

-- Информация о блоках
CREATE TABLE blockchain_database.block (
  block_hash varchar(64) PRIMARY KEY,
  miner_key varchar(64) NOT NULL REFERENCES blockchain_database.miner(miner_pub_key),
  block_dttm TIMESTAMP NOT NULL,
  height INTEGER NOT NULL,
  reward_coin INTEGER NOT NULL REFERENCES blockchain_database.coin(coin_id),
  reward_value DOUBLE PRECISION NOT NULL,
  difficulty DOUBLE PRECISION NOT NULL
);


-- Информация об операциях
CREATE TABLE blockchain_database.operation (
  operation_hash varchar(64) PRIMARY KEY,
  from_node_key varchar(64) NOT NULL REFERENCES blockchain_database.node(node_pub_key),
  to_node_key varchar(64) NOT NULL REFERENCES blockchain_database.node(node_pub_key),
  coin_id INTEGER NOT NULL REFERENCES blockchain_database.coin(coin_id),
  value DOUBLE PRECISION NOT NULL,
  operation_dttm TIMESTAMP NOT NULL,
  block_hash varchar(64) REFERENCES blockchain_database.block(block_hash),
  fee DOUBLE PRECISION NOT NULL
)

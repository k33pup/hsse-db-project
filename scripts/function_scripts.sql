-- Триггер для автоматического увеличения счетчика операций узла при добавлении новой операции в таблицу blockchain_database.operation
CREATE OR REPLACE FUNCTION increment_operations_count()
RETURNS TRIGGER AS $$
BEGIN
  UPDATE blockchain_database.node
  SET operations_cnt = operations_cnt + 1
  WHERE node_pub_key = NEW.from_node_key;

  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER increment_operations_trigger
AFTER INSERT ON blockchain_database.operation
FOR EACH ROW
EXECUTE FUNCTION increment_operations_count();


--- Триггер для автоматического увеличения количества добытых блоков у майнера при добавлении нового блока в таблицу blockchain_database.block
CREATE OR REPLACE FUNCTION increment_blocks_mined()
RETURNS TRIGGER AS $$
BEGIN
  UPDATE blockchain_database.miner
  SET block_mined_cnt = block_mined_cnt + 1
  WHERE miner_pub_key = NEW.miner_key;

  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER increment_blocks_mined_trigger
AFTER INSERT ON blockchain_database.block
FOR EACH ROW
EXECUTE FUNCTION increment_blocks_mined();


--- Функция для вычисления хеша всех транзакций для определенного блока
CREATE OR REPLACE FUNCTION calculate_block_transactions_hash(cur_block_hash varchar(64))
RETURNS VARCHAR AS $$
DECLARE
    transactions_hash TEXT;
BEGIN
    SELECT STRING_AGG(operation_hash, '') INTO transactions_hash
    FROM blockchain_database.operation
    WHERE block_hash = cur_block_hash;

    RETURN ENCODE(DIGEST(transactions_hash, 'sha256'), 'hex');
END;
$$ LANGUAGE plpgsql;

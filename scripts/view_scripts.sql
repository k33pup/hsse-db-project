--- Отображение информации о блоках, включая детали о майнере и награде за блок
CREATE VIEW block_details AS
SELECT b.block_hash, b.block_dttm, b.height, m.miner_pub_key, m.block_mined_cnt, m.miner_power, c.coin_name, c.coin_project_url, b.reward_value, b.difficulty
FROM blockchain_database.block b
JOIN blockchain_database.miner m ON b.miner_key = m.miner_pub_key
JOIN blockchain_database.coin c ON b.reward_coin = c.coin_id;

--- Отображение суммарной стоимости всех операций по каждой монете
CREATE VIEW total_operations_value AS
SELECT c.coin_name, SUM(o.value) AS total_value
FROM blockchain_database.operation o
JOIN blockchain_database.coin c ON o.coin_id = c.coin_id
GROUP BY c.coin_name;

--- Отображение информации о самых активных майнерах по количеству добытых блоков
CREATE VIEW top_miners AS
SELECT m.miner_pub_key, m.block_mined_cnt, m.miner_power, n.join_date
FROM blockchain_database.miner m
JOIN blockchain_database.node n ON m.miner_pub_key = n.node_pub_key
ORDER BY m.block_mined_cnt DESC
LIMIT 3;

--- Отображение сводную информацию о количестве операций, общей сумме отправленных значений и средней комиссии для каждого узла
CREATE VIEW node_operations_summary AS
SELECT n.node_pub_key, COUNT(o.operation_hash) AS total_operations,
       SUM(o.value) AS total_value_sent,
       AVG(o.fee) AS avg_fee
FROM blockchain_database.node n
LEFT JOIN blockchain_database.operation o ON n.node_pub_key = o.from_node_key
GROUP BY n.node_pub_key
ORDER BY total_operations DESC;


--- Неоконные select

SELECT operation_hash, value
FROM blockchain_database.operation
WHERE coin_id > 6;

SELECT coin_id, AVG(value) as avg_value
FROM blockchain_database.operation
GROUP BY coin_id
HAVING AVG(value) > 100;

SELECT miner_key, COUNT(DISTINCT block_hash) as mined_cnt
FROM blockchain_database.block
GROUP BY miner_key
ORDER BY mined_cnt DESC
LIMIT 1;

SELECT coin_name, coin_project_url
FROM blockchain_database.coin
WHERE LENGTH(coin_name) > 7;

SELECT node_pub_key, sum(operations_cnt) as sum_cnt
FROM blockchain_database.node
WHERE join_date BETWEEN '2001-01-01' AND '2007-01-01'
GROUP BY node_pub_key;

--- Оконные select

SELECT miner_pub_key,
       block_mined_cnt,
       DENSE_RANK() over w AS rank
FROM blockchain_database.miner
WINDOW w AS (
    ORDER BY block_mined_cnt DESC
)
ORDER BY rank;


SELECT sum(value) OVER w AS block_sum, block_hash
FROM blockchain_database.operation
WINDOW w AS (
    PARTITION BY block_hash
);

WITH precalc AS (
    SELECT node_pub_key, operations_cnt, lag(operations_cnt, 1) OVER w AS prev
    FROM blockchain_database.node
    WINDOW w AS (
        ORDER BY operations_cnt
    )
)
SELECT node_pub_key, operations_cnt, operations_cnt - prev AS diff
FROM precalc;


SELECT operation_hash, sum(value) OVER w AS total_sum
FROM blockchain_database.operation
WINDOW w AS (
    ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
);


SELECT operation_hash, coin_id, value,  DENSE_RANK() OVER w AS operation_ind
FROM blockchain_database.operation
WINDOW w AS (
    PARTITION BY coin_id
    ORDER BY value
);

--- Обновление таблицы coin
UPDATE blockchain_database.coin SET coin_name = 'Btc', coin_project_url = 'https://bitcoin.com' WHERE coin_id = 2;
UPDATE blockchain_database.coin SET coin_name = 'Toncoin', coin_project_url = 'https://toncoin.com' WHERE coin_id = 5;

--- Обновление таблицы node
UPDATE blockchain_database.node SET node_private_key = 'ead446189d45d915a81be366179b769b85957a0682e5ac3ca085b906894bc6fc'
WHERE node_pub_key = '0e4f9ea7ce6d21fade30c490be01e66a43eea0ba4e72b2ffa41fbe91a619d739';
UPDATE blockchain_database.node SET join_date = '2004-04-05 10:00:00'
WHERE node_pub_key = '2c6f2b6ea2dad9085f7a95e438d461f1a7f497cbdf76f2b8734ee93094fd1c1f';

--- Обновление таблицы operation
DELETE FROM blockchain_database.operation WHERE operation_hash = '862eb53bb657fac5998f00467d490671d6982a669071b90403c8d32683274882';
DELETE FROM blockchain_database.operation WHERE from_node_key = '2c6f2b6ea2dad9085f7a95e438d461f1a7f497cbdf76f2b8734ee93094fd1c1f' AND
                                                to_node_key = '890fe72ea60a408444a3a348b1d1968e417bd42e3351b80090d3c0e0719c4b58' AND
                                                coin_id = 10;

--- Обновление таблицы block
UPDATE blockchain_database.block SET reward_coin = 10 WHERE block_dttm = '2025-04-30 18:05:34.000000';
UPDATE blockchain_database.block SET difficulty = 123123123 WHERE block_hash = 'f38d4d9e677b0926e7c823fe95161243d9a1a80c474f39fd1ce53378be770c2f';
